package cardoso.ricardo.topheadlines.View.Views.HeadlinesList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cardoso.ricardo.topheadlines.Controller.AsyncTasks.GetHeadlinesAsyncTask;
import cardoso.ricardo.topheadlines.Model.AppConstants;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Article;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import cardoso.ricardo.topheadlines.Model.Domain.RequestResponse;
import cardoso.ricardo.topheadlines.Model.Enums.StatusResponse;
import cardoso.ricardo.topheadlines.R;
import cardoso.ricardo.topheadlines.View.Helper.AuxMethods;

import java.util.HashMap;
import java.util.Map;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ListHeadlinesFragment extends Fragment implements  ListHeadlinesFragmentCommunication{
    private static final String DEFAULT_VALUE_TO_SEARCH = "bbc-news";

    private OnListFragmentInteractionListener mListener;
    
    //aux variable to set adapter when data is received
    private RecyclerView recyclerView;

    private Headlines headlines ;
    private GetHeadlinesAsyncTask getHeadlinesAsyncTask;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListHeadlinesFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Map<String,String> myMap = new HashMap<String,String>();
        myMap.put(AppConstants.NEWS_API_REQ_PARAMS_SOURCES, DEFAULT_VALUE_TO_SEARCH);
        executeAsyncTask(myMap);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listheadlines_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void createGetHeadlinesAsyncTask(){
        getHeadlinesAsyncTask = new GetHeadlinesAsyncTask(){
            @Override
            protected void onPostExecute(RequestResponse<Headlines> headlinesReceived) {
                super.onPostExecute(headlinesReceived);
                if(headlinesReceived.getStatusResponse() == StatusResponse.SUCCESS)
                {
                    headlines = headlinesReceived.getData();
                    if(headlines != null && headlines.articles != null){
                        headlines.articles = AuxMethods.sortArticles(headlines.articles);
                    }

                    if(recyclerView != null && headlines != null){
                        recyclerView.setAdapter(new MyListHeadlinesRecyclerViewAdapter(headlines.articles, mListener));
                    }
                    else {
                        showImpossibleToGetDataToast();
                    }
                }
                else {
                    showImpossibleToGetDataToast();
                }
            }
        };
    }

    private void showImpossibleToGetDataToast() {
        Toast.makeText(getActivity().getApplicationContext(), R.string.impossible_to_obtain_data, Toast.LENGTH_SHORT).show();
    }

    //we need to create a new asyncTask first, can't use async Tasks that had been used previously
    private void executeAsyncTask(Map<String, String> map){
        createGetHeadlinesAsyncTask();
        getHeadlinesAsyncTask.execute(map);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Article item);
    }

    @Override
    public void getHeadlines(Map<String, String> map) {
        executeAsyncTask(map);
    }
}
