package cardoso.ricardo.topheadlines.View.Views.Article;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import cardoso.ricardo.topheadlines.Model.AppConstants;
import cardoso.ricardo.topheadlines.R;
import cardoso.ricardo.topheadlines.View.Parcelables.ArticleParcelable;

public class ArticleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        ArticleParcelable article = (ArticleParcelable) getIntent().getParcelableExtra(AppConstants.EXTRA_ARTICLE);

        ImageView image = (ImageView)findViewById(R.id.act_article_headline_image);
        TextView title = (TextView)findViewById(R.id.act_article_headline_title);
        TextView description = (TextView)findViewById(R.id.act_article_headline_description);
        TextView content = (TextView)findViewById(R.id.act_article_headline_content);

        Glide
                .with(image)
                .load(article.urlToImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image);

        title.setText(article.title);
        description.setText(article.description);
        content.setText(article.content);
    }
}
