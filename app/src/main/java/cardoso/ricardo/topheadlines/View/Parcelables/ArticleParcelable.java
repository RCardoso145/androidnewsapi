package cardoso.ricardo.topheadlines.View.Parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Article;

public class ArticleParcelable implements Parcelable {
    public String title;
    public String urlToImage;
    public String description;
    public String content;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(urlToImage);
        dest.writeString(description);
        dest.writeString(content);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<ArticleParcelable> CREATOR = new Parcelable.Creator<ArticleParcelable>() {
        public ArticleParcelable createFromParcel(Parcel in) {
            return new ArticleParcelable(in);
        }

        public ArticleParcelable[] newArray(int size) {
            return new ArticleParcelable[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private ArticleParcelable(Parcel in) {
        title = in.readString();
        urlToImage = in.readString();
        description = in.readString();
        content = in.readString();
    }

    public ArticleParcelable(Article article){
        title = article.title;
        urlToImage = article.urlToImage;
        description = article.description;
        content = article.content;
    }
}
