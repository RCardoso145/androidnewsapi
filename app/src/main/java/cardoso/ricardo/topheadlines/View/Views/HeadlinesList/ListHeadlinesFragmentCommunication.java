package cardoso.ricardo.topheadlines.View.Views.HeadlinesList;

import java.util.Map;

public interface ListHeadlinesFragmentCommunication {
    void getHeadlines(Map<String, String> map);
}
