package cardoso.ricardo.topheadlines.View.Views.HeadlinesList;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import cardoso.ricardo.topheadlines.Model.AppConstants;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Article;
import cardoso.ricardo.topheadlines.R;
import cardoso.ricardo.topheadlines.View.Helper.AuxMethods;
import cardoso.ricardo.topheadlines.View.Parcelables.ArticleParcelable;
import cardoso.ricardo.topheadlines.View.Views.Article.ArticleActivity;

public class MainActivity extends AppCompatActivity implements ListHeadlinesFragment.OnListFragmentInteractionListener {

    static final String TITLE = "TopHeadlines";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edit_text = (EditText) findViewById(R.id.editTextSources);
        Button button_get_sources = (Button) findViewById(R.id.buttonGetSources);

        ListHeadlinesFragmentCommunication articleFrag = (ListHeadlinesFragment)
                getSupportFragmentManager().findFragmentById(R.id.activity_main_fragment_headlines_list);

        button_get_sources.setOnClickListener(v -> {
            if(AuxMethods.isNetworkConnected(this)){
                if(!AuxMethods.stringNullOrEmpty(edit_text.getText().toString())){
                    Map<String,String> myMap = new HashMap<String,String>();
                    myMap.put(
                            AppConstants.NEWS_API_REQ_PARAMS_SOURCES,
                            AuxMethods.convertToSourcesString(edit_text.getText().toString()));
                    articleFrag.getHeadlines(myMap);
                    setTitle(TITLE + ": " + edit_text.getText().toString());
                }
            }
            else {
                Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onListFragmentInteraction(Article item) {
        Intent intent = new Intent(this, ArticleActivity.class);
        intent.putExtra(AppConstants.EXTRA_ARTICLE, new ArticleParcelable(item));
        startActivity(intent);
    }
}
