package cardoso.ricardo.topheadlines.View.Helper;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.Collections;
import java.util.List;

import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Article;

public class AuxMethods {
    private static final String SPLIT_STR = " ", SEPARATOR = ",";

    //converts a string to a string with the correct news api sources format
    public static String convertToSourcesString(String str){
        String toReturn = "";
        if(!stringNullOrEmpty(str)){
            String[] parts = str.split(SPLIT_STR);
            for (int i = 0 ; i< parts.length; i++) {
                toReturn += parts[i];
                if(i != parts.length-1){ //if is not the last one
                    //adds a separator
                    toReturn += SEPARATOR;
                }
            }
        }
        return toReturn;
    }

    public static boolean stringNullOrEmpty(final String string){
        return string == null || string.trim().isEmpty();
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static List<Article> sortArticles(List<Article> articles) {
        Collections.sort(articles, (h1, h2) -> {
            if(h1 != null && h2 != null){
                return h1.publishedAt.after(h2.publishedAt) ? -1 : 1;
            }
            return 0;
        });
        return articles;
    }
}
