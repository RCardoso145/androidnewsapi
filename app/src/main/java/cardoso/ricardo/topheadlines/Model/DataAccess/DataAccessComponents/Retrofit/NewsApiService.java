package cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessComponents.Retrofit;

import java.util.Map;

import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface NewsApiService {

    //https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=4ee619cb6c05484a8ff0480fe2246d0b
    @GET("top-headlines")
    Call<Headlines> getHeadlines(@QueryMap Map<String, String> options);
}
