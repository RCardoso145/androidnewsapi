package cardoso.ricardo.topheadlines.Model.Domain;

import cardoso.ricardo.topheadlines.Model.Enums.StatusResponse;

public class RequestResponse<T> {
    private T data;
    private StatusResponse statusResponse;

    public RequestResponse(T data, StatusResponse statusResponse) {
        this.statusResponse = statusResponse;
        this.data = data;
    }

    public StatusResponse getStatusResponse(){ return statusResponse; }
    public T getData() { return data; }
}
