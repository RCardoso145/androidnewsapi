package cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessInterface;

import java.util.Map;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import cardoso.ricardo.topheadlines.Model.Domain.RequestResponse;

public interface ICrudHeadlines {

    RequestResponse<Headlines> getHeadlines(Map<String, String> map);
}
