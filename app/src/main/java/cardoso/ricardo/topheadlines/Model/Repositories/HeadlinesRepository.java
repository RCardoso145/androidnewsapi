package cardoso.ricardo.topheadlines.Model.Repositories;

import java.util.Map;

import cardoso.ricardo.topheadlines.Controller.ApplicationNewsApi;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import cardoso.ricardo.topheadlines.Model.Domain.RequestResponse;

public class HeadlinesRepository {

    public RequestResponse<Headlines> GetHeadlines(Map<String, String> map){
        return ApplicationNewsApi.dataContext.getCrudHeadlines().getHeadlines(map);
    }
}
