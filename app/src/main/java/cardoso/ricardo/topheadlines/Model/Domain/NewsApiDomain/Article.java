package cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Article {

    @SerializedName("title")
    public String title;

    @SerializedName("publishedAt")
    public Date publishedAt;

    @SerializedName("urlToImage")
    public String urlToImage;

    @SerializedName("description")
    public String description;

    @SerializedName("content")
    public String content;

}
