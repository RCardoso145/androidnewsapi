package cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessComponents.Retrofit;

import java.io.IOException;
import java.util.Map;

import cardoso.ricardo.topheadlines.Model.AppConstants;
import cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessInterface.ICrudHeadlines;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import cardoso.ricardo.topheadlines.Model.Domain.RequestResponse;
import cardoso.ricardo.topheadlines.Model.Enums.StatusResponse;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitCrudHeadlines implements ICrudHeadlines {

    private Retrofit retrofit;

    public RetrofitCrudHeadlines(){
        retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.NEWS_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public RequestResponse<Headlines> getHeadlines(Map<String, String> map) {
        Headlines headlines = null;
        NewsApiService service = retrofit.create(NewsApiService.class);
        Call<Headlines> repos = service.getHeadlines(map);
        try{
            Response<Headlines> response = repos.execute();
            if(response.isSuccessful()){
                return new RequestResponse<Headlines>(response.body(), StatusResponse.SUCCESS);
            }
            else {
                return new RequestResponse<Headlines>(response.body(), StatusResponse.ERROR);
            }
        }
        catch(IOException e ){
            return new RequestResponse<Headlines>(null, StatusResponse.ERROR);
        }
    }
}
