package cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Headlines {

    @SerializedName("status")
    public String status;

    @SerializedName("articles")
    public List<Article> articles;
}
