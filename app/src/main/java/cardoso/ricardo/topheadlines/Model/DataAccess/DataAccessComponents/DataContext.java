package cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessComponents;

import cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessInterface.ICrudHeadlines;

public class DataContext {

    private static ICrudHeadlines crudHeadlines;

    public static void setCrudHeadlines(ICrudHeadlines crudHeadlinesReceived) {
        crudHeadlines = crudHeadlinesReceived;
    }

    public static ICrudHeadlines getCrudHeadlines() {
        return crudHeadlines;
    }
}
