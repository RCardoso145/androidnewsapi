package cardoso.ricardo.topheadlines.Model;

public class AppConstants {

    //should be encripted and stored in a safer way
    public static final String API_KEY = "4ee619cb6c05484a8ff0480fe2246d0b";
    public static final String NEWS_API_URL = "https://newsapi.org/v2/";

    public static final String NEWS_API_REQ_PARAMS_SOURCES = "sources";
    public static final String NEWS_API_REQ_PARAMS_APIKEY = "apiKey";
    public static final String EXTRA_ARTICLE = "EXTRA_ARTICLE";
}
