package cardoso.ricardo.topheadlines.Controller;

import android.app.Application;

import cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessComponents.DataContext;
import cardoso.ricardo.topheadlines.Model.DataAccess.DataAccessComponents.Retrofit.RetrofitCrudHeadlines;

public class ApplicationNewsApi extends Application {
    public static DataContext dataContext = new DataContext();

    @Override
    public void onCreate() {
        super.onCreate();
        dataContext.setCrudHeadlines(new RetrofitCrudHeadlines());
    }
}
