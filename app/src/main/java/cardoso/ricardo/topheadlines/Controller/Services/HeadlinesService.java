package cardoso.ricardo.topheadlines.Controller.Services;

import java.util.Map;

import cardoso.ricardo.topheadlines.Model.AppConstants;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import cardoso.ricardo.topheadlines.Model.Domain.RequestResponse;
import cardoso.ricardo.topheadlines.Model.Repositories.HeadlinesRepository;

public class HeadlinesService {

    HeadlinesRepository headlinesRepository;

    public HeadlinesService() {
        headlinesRepository = new HeadlinesRepository();
    }

    public RequestResponse<Headlines> getHeadlines(Map<String, String> map){
        if(map!= null){
            verifyApiKeyInMap(map);
            return headlinesRepository.GetHeadlines(map);
        }
        return null;
    }

    private void verifyApiKeyInMap(Map<String, String> map) {
        if(! map.containsKey(AppConstants.NEWS_API_REQ_PARAMS_APIKEY)){
            map.put(AppConstants.NEWS_API_REQ_PARAMS_APIKEY, AppConstants.API_KEY);
        }
    }
}
