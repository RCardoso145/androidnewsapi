package cardoso.ricardo.topheadlines.Controller.AsyncTasks;

import android.os.AsyncTask;

import java.util.Map;

import cardoso.ricardo.topheadlines.Controller.Services.HeadlinesService;
import cardoso.ricardo.topheadlines.Model.Domain.NewsApiDomain.Headlines;
import cardoso.ricardo.topheadlines.Model.Domain.RequestResponse;
import cardoso.ricardo.topheadlines.Model.Enums.StatusResponse;

public class GetHeadlinesAsyncTask extends AsyncTask<Map<String, String>, Integer, RequestResponse<Headlines>> {
    @Override
    protected RequestResponse<Headlines> doInBackground(Map<String, String>... maps) {

        if(maps.length>=1){
            return new HeadlinesService().getHeadlines(maps[0]);
        }
        return new RequestResponse<Headlines>(null, StatusResponse.ERROR);
    }
}
