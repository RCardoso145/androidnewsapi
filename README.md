# AndroidNewsApi

**Unfortunately I didn't had time to:**
*  Include unit tests;
*  Check if the app supports both portrait and landscape modes;

**One thing that I whould add if I had time:**
*  Create a service that makes a request to the api with information of the available terms to search (https://newsapi.org/v2/sources?apiKey=4ee619cb6c05484a8ff0480fe2246d0b) and stores that data, to be showed in a spinner (for example), instead of letting the user write the sources.

**Source examples to search in the app:**
*  abc-news;
*  ansa;
*  argaam;
*  axios;
*  bbc-news.